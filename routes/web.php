<?php
use Laratube\Http\Controllers\UploadVideoController;
use Laratube\Http\Controllers\VideoController;
use Laratube\Http\Controllers\VoteController;
use Laratube\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('channels', 'ChannelController');
Route::get('/drafts/new', 'Auth\PostController@index')->name('drafts.new');

Route::get('/', function () {
    return view('top'); // この行を編集
});

Route::get('/post_index', 'PostsController@index');

Route::get('/article/add', 'ArticleController@add')->name('article_add');
Route::post('/article/add', 'ArticleController@create')->name('article_create');

//Route::post('/article/add','MainController@write1')->name('article_add');
// ここに追加
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', 'ContactController@form');
Route::post('/contact/confirm', 'ContactController@confirm');
Route::post('/contact/process', 'ContactController@process');


Route::get('videos/{video}', [VideoController::class, 'show'])->name('videos.show');
Route::put('videos/{video}', [VideoController::class, 'updateViews']);
Route::get('videos/{video}/comments', [CommentController::class, 'index']);
Route::get('comments/{comment}/replies', [CommentController::class, 'show']);
Route::put('videos/{video}/update', [VideoController::class, 'update'])->middleware(['auth'])->name('videos.update');

Route::get('/posts/{id}', 'PostsController@show');

Route::middleware(['auth'])->group(function () {
    Route::post('comments/{video}', [CommentController::class, 'store']);
    Route::post('votes/{entityId}/{type}', [VoteController::class, 'vote']);
    Route::post('channels/{channel}/videos', [UploadVideoController::class, 'store']);
    Route::get('channels/{channel}/videos', [UploadVideoController::class, 'index'])->name('channel.upload');
    Route::resource('channels/{channel}/subscriptions', 'SubscriptionController')->only(['store', 'destroy']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
