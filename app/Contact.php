<?php

namespace Oshigoto46;

use IlluminateDatabaseEloquentModel;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'message',
    ];
}