<?php

namespace Oshigoto46\Http\Controllers;

use Illuminate\Http\Request;
use Oshigoto46\Contact;

class ContactController extends Controller
{
    
    
    public function form()
    {
        return view('form');
    }
    public function confirm(Request $request)
    {
        $contact2 = new Contact($request->all());

        return view('confirm2', compact('contact2'));
    }
    public function process(Request $request)
    {
        return view('complete');
    }
}
