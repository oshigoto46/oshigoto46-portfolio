<?php

namespace Oshigoto46\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show($id) {
        $post = Post::findOrFail($id);
        return view('posts.show')->with('post', $post);
    }
}
