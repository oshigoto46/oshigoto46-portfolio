<?php

namespace Oshigoto46;

class Vote extends Model
{
    public function voteable()
    {
        return $this->morphTo();
    }
}
