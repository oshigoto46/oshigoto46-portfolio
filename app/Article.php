<?php

namespace Oshigoto46;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = ['user_id', 'title', 'content'];
}
