<?php

use Faker\Generator as Faker;
use Oshigoto46\Comment;
use Oshigoto46\Video;
use Oshigoto46\User;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->sentence(6),
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'video_id' => function() {
            return factory(Video::class)->create()->id;
        },
        'comment_id' => null
    ];
});
