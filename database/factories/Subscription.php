<?php

use Faker\Generator as Faker;
use Oshigoto46\User;
use Oshigoto46\Subscription;
use Oshigoto46\Channel;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'channel_id' => function() {
            return factory(Channel::class)->create()->id;
        },
    ];
});
